# madlibs-CLI

### Summary
Simple Node madlib game that uses a constructor to create a madlib story out of user input. Utilizes inquirer to accept user input.

### Getting Started
* Clone repository to the directory of your choice
* Run ` npm install `
* Run ` node game `

### Playing the game
* Follow the prompts for entering a noun, adjective, adverb, and verb.
* A madlib story will be returned with your input.

### Screenshots

Responding to prompts

![responding to prompts](images/input.png)

Returned madlib story

![returned madlib story](images/output.png)
