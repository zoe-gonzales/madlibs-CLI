
// file containing madlib constructor
// Defines nouns, adjectives, adverbs, and verbs as input by user
// Also defines the madlib story to be created

function MadLib(nouns, adjectives, adverbs, verbs) {
    this.nouns = nouns;
    this.adjectives = adjectives;
    this.adverbs = adverbs;
    this.verbs = verbs;
    this.story = `Once upon a time, there was a [noun] where all the [noun]s loved to [verb]. In the morning, they would wake [adverb] and begin the day with a [noun]. "We are so [adjective] to be [adjective]," they would say. The sun would shine and they would [verb] together in [adjective] [noun].`
}

module.exports = MadLib;