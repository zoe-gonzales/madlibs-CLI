
// importing inquirer and Madlib constructor
var inquirer = require('inquirer');
var MadLib = require('./madlib');

// file containing logic of the game

// arrays to hold player input
var nouns = [];
var adjectives = [];
var adverbs = [];
var verbs = [];
// count keeps track of how many times inquirer should prompt player for input
var count = 0;

// retrieves input from player and added to above arrays
function saveWords(count) {
    // prompting for nouns
    // reruns function until 4 nouns have been saved
    if (count < 4) {
        inquirer
        .prompt({
            name: 'noun',
            message: 'Enter a noun:'
        }).then(function(reply){
            nouns.push(reply.noun);
            count++;
            saveWords(count);
        });
    }
    // prompting for adjectives - runs until there are 3 adjectives saved
    if (count >= 4 && count < 7) {
        inquirer
        .prompt({
            name: 'adjective',
            message: 'Enter an adjective'
        }).then(function(reply){
            adjectives.push(reply.adjective);
            count++;
            saveWords(count);
        });
    }
    // prompting for an adverb - runs until there is one adverb saved
    if (count >=7 && count < 8) {
        inquirer
        .prompt({
            name: 'adverb',
            message: 'Enter an adverb'
        }).then(function(reply){
            adverbs.push(reply.adverb);
            count++;
            saveWords(count);
        });
    } 
    // prompting for verbs - runs until there are 2 verbs saved
    if (count >= 8 && count < 10) {
        inquirer
        .prompt({
            name: 'verb',
            message: 'Enter an verb'
        }).then(function(reply){
            verbs.push(reply.verb);
            count++;
            saveWords(count);
        });
    }
    // once all input is saved, an instance of the Madlib constructor is created
    if (count === 10) {
        var madlib = new MadLib(nouns, adjectives, adverbs, verbs);
        // calls create story function on the new object
        createStory(madlib);
    }
}

saveWords(count);

// function replaces player input with specified sections of the object.story string
var createStory = function(obj) {
    // replacing nouns
    for (var i=0; i < 4; i++){
        obj.story = obj.story.replace('[noun]', obj.nouns[i]);
    }
    // replacing adjectives
    for (var i=0; i < 3; i++){
        obj.story = obj.story.replace('[adjective]', obj.adjectives[i]);
    }
    // replacing adverb
    for (var i=0; i < 1; i++){
        obj.story = obj.story.replace('[adverb]', obj.adverbs[i]);
    }
    // replacing verbs
    for (var i=0; i < 2; i++){
        obj.story = obj.story.replace('[verb]', obj.verbs[i]);
    }
    // prints new object.story string to console
    console.log(obj.story);
}